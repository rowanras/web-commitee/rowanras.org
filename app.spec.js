const request = require('supertest');
const should = require('chai').should();
const process = require('process');

process.env.PORT = 1234
process.env.NODE_ENV = 'test'

describe('Serve pages', function () {
  let app;
  beforeEach(function () {
    app = require('./app');
  });
  afterEach(function () {
    app.close();
  });
  it('should serve home page', done => shouldServePage('/', app, done));
  it('should serve about page', done => shouldServePage('/about', app, done));
  it('should serve news page', done => shouldServePage('/news', app, done));
  it('should serve events page', done =>  shouldServePage('/events', app, done));
  it('should serve eboard page', done =>  shouldServePage('/eboard', app, done));
  it('should serve eboard years', done =>  shouldServePage('/eboard#2019', app, done));
  it('should serve get involved page', done =>  shouldServePage('/get_involved', app, done));
  it('should serve suggestion box page', done =>  shouldServePage('/suggestion_box', app, done));

  it('redirects to 404 page on invalid page', done => {
    request(app)
      .get('/not/a/page')
      .expect(302)
      .expect('Location', '/page_not_found')
      .end(done)
  });
});

function shouldServePage(page, app, done) {
  request(app).get(page).expect(200, done)
}
