const express = require('express');
const path = require('path');
const { env } = require('process');

const app = express();


if (env.NODE_ENV === 'production') {
  app.get('*', (req, res, next) => {
    if (req.headers['x-forwarded-proto'] === 'https') {
      return next();
    }
    res.redirect(`https://${req.headers.host}${req.url}`);
  });
}

app.use(express.static(path.join(__dirname, 'dist'), { extensions: ['html'] }));

app.get('*', (req, res) => {
  return res.redirect('/page_not_found')
})
const server = app.listen(env.PORT || 3000);

module.exports = server;
